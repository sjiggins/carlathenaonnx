# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration

# Declare the package's name.
atlas_subdir( CARLAthena )

# Declare the packages dependencies
atlas_depends_on_subdirs(
  PUBLIC
  GaudiKernel
  Tools/PathResolver
  )

# Find the necessary external(s).
find_package( ROOT COMPONENTS Core Hist Tree Physics )

# Build the package's main library.
atlas_add_component( CARLAthena
   src/*.h src/*.cxx src/components/*.cxx
   INCLUDE_DIRS ${ROOT_INCLUDE_DIRS} ${ONNXRUNTIME_INCLUDE_DIRS}
   LINK_LIBRARIES ${ROOT_LIBRARIES} ${ONNXRUNTIME_LIBRARIES}
   AthAnalysisBaseCompsLib xAODMuon xAODTruth xAODMissingET xAODJet GaudiKernel PathResolver xAODEventInfo)

# If OnnxRuntime is built as part of this project, and not taken from somewhere
# on the system, then explicitly wait with the build of this library for the
# completion of the OnnxRuntime build.
if( NOT ONNXRUNTIME_FOUND )
   add_dependencies( CARLAthena onnxruntime )
endif()

# Install files from the package.
atlas_install_python_modules( python/*.py )
atlas_install_joboptions( share/*.py )
atlas_install_data( data/* )
