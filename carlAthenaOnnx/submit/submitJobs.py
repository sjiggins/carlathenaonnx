import os
import sys


def submitJob(jobOptions, username, suffix, dataset):
    """
    Function that create GRID submision termnial command

    params:
        jobOptions (str) : name of the JobOptions for GRID submission.
            e.g. 'CARLAthenaAlgJobOptions'

        username (str) : username for GRID submission.

        suffix (str) : output file name suffix.

        dataset (str) : name of the input data set.
    """

    ##Athena Job Options
    com = "pathena CARLAthena/{}.py ".format(jobOptions)

    ## Notify Panda that submission will use new code package
    com += "--useNewCode "

    # it would be nice to use fstring, but I think athena is python2?
    ## Notify panda to use input datasets in configuration file
    com += "--inDS {} ".format(dataset)

    ## Notify panda that I will either use user rights or official production role
    oDS = "user.{}.{}.{}".format(username, dataset.replace("/", ""), suffix)

    print("{} has length: {}".format(oDS, len(oDS)))
    while len(oDS) > 115:
        print("{} too long!!!".format(len(oDS)))
        splODS = oDS.split("_")
        splODS.pop(2)
        oDS = "_".join(splODS)
        pass
    print("final: {} has length: {}".format(oDS, len(oDS)))
    com += "--outDS {} ".format(oDS)

    ##Optional output name (ignore)
    # com += " --extOutFile ntuple.root "

    ## Unknown option at this point, but lets keep it.
    com += " --supStream GLOBAL "

    ## ptions for each job submission
    # com += " --nEventsPerJob 25000 "
    # com += " --nFilesPerJob 1"

    ## Notify panda submission to merge all sub-jobs at GRID site.
    com += " --mergeOutput "
    ## Return the command to be executed
    return com


#################################################################################

if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description="GRID submittion for CARL Athena")
    parser.add_argument(
        "--suffix", dest="suffix", type=str, help="suffix used in GRID outputs."
    )
    parser.add_argument(
        "--user", dest="user", type=str, help="Username for GRID submission."
    )
    parser.add_argument(
        "--input",
        dest="input",
        type=str,
        help="Path to a txt file that contains list of samples.",
    )
    parser.add_argument(
        "--jobOptions", dest="jobOptions", type=str, help="Name of the JobOptions."
    )
    cml_args = parser.parse_args()

    # parsing the input file
    with open(cml_args.input, "r") as inputFile:
        file_list = inputFile.readlines()
        file_list = [f.replace("\n", "") for f in file_list]
        file_list = [f.replace(" ", "") for f in file_list]

    # loop through file_list and submit jobs
    for ds in file_list:
        if "#" in ds:
            continue
        if all(x not in ds for x in ["mc", "valid", "group", "data"]):
            continue
        print("prepare subitssion for: {}".format(ds))
        command = submitJob(cml_args.jobOptions, cml_args.user, cml_args.suffix, ds)
        print(command)
        os.system(command)
