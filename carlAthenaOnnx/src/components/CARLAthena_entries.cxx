
#include "GaudiKernel/DeclareFactoryEntries.h"

#include "../CARLAthenaAlg.h"
#include "../ZjetsAlg.h"

DECLARE_ALGORITHM_FACTORY( CARLAthenaAlg )
DECLARE_ALGORITHM_FACTORY( ZjetsAlg )

DECLARE_FACTORY_ENTRIES( CARLAthena )
{
  DECLARE_ALGORITHM( CARLAthenaAlg );
  DECLARE_ALGORITHM( ZjetsAlg );
}
