#ifndef CARLATHENA_CARLATHENAALG_H
#define CARLATHENA_CARLATHENAALG_H 1

#include "AthAnalysisBaseComps/AthAnalysisAlgorithm.h"

// ROOT includes
#include <TTree.h>
#include <TLeaf.h>
#include <vector>
#include <TH1.h>
#include <TLorentzVector.h>

// Basic C++ STD library includes
#include <map>
#include <iostream>
#include <memory>
#include <fstream>

// xAOD includes
#include <xAODTruth/TruthParticleContainer.h>
#include <xAODTruth/TruthParticle.h>
#include <xAODJet/Jet.h>
#include <xAODJet/JetContainer.h>
#include <xAODEventInfo/EventInfo.h>

// OnnxRuntime include(s).
//#include <core/session/onnxruntime_cxx_api.h>

// CARLNN includes
#include "CARLNN.h"

class CARLAthenaAlg: public ::AthAnalysisAlgorithm
{
 public:
  CARLAthenaAlg(const std::string &name, ISvcLocator *pSvcLocator );
  virtual ~CARLAthenaAlg();

  //AthAnalysisAlgorithm base components
                                                 //IS EXECUTED:
  virtual StatusCode  initialize() override;     //once, before any input is loaded
  virtual StatusCode  beginInputFile() override; //start of each input file, only metadata loaded
  virtual StatusCode  execute() override;        //per event
  virtual StatusCode  finalize() override;       //once, after all events processed

  // Data members for events or samples
  unsigned int m_runNumber = 0; ///< Run number
  unsigned long long m_eventNumber = 0; ///< Event number

  ///Other useful methods provided by base class are:
  ///evtStore()        : ServiceHandle to main event data storegate
  ///inputMetaStore()  : ServiceHandle to input metadata storegate
  ///outputMetaStore() : ServiceHandle to output metadata storegate
  ///histSvc()         : ServiceHandle to output ROOT service (writing TObjects)
  ///currentFile()     : TFile* to the currently open input file
  ///retrieveMetadata(...): See twiki.cern.ch/twiki/bin/view/AtlasProtected/AthAnalysisBase#ReadingMetaDataInCpp

 private:

   // these are variables for declareProperty
    bool m_doDebug = false;
    bool m_doInference = false;
    //std::string m_sampleName;
    std::string m_treePrefix;
    std::string m_treeDescription;

    //TTree variables
    TTree* m_tree;

    // histogram for storing the sum of MC event weights
    TH1D *m_sumWeight;
    float m_eventWeight = 0;
    
    // Needed to store CARL weight
    float m_carl_weight;

    // boson variables
    float m_VpT = 0;
    float m_Veta = 0;
    float m_Vmass = 0;

    // jet variables
    int m_Njets = 0;
    float m_HT = 0;
    std::vector<float> m_Jet_Pt;
    std::vector<float> m_Jet_Eta;
    std::vector<float> m_Jet_Phi;
    std::vector<float> m_Jet_Mass;

    // lepton variables
    int m_nLeptons=0;
    std::vector<float> m_Lepton_Pt;
    std::vector<float> m_Lepton_Eta;
    std::vector<float> m_Lepton_Phi;
    std::vector<float> m_Lepton_Mass;
    std::vector<int> m_Lepton_ID;

    // xAOD containers
    const xAOD::TruthParticleContainer *m_truthParticles = 0;
    const xAOD::JetContainer *m_truthJets = 0;
    const xAOD::EventInfo *m_eventInfo=0;

    // other variables
    int m_event_counter = 0;

    // custom methods
    void TreeFill();
    bool PassLeptonEtaPtCuts(const xAOD::TruthParticle *particle, const double pt, const double eta);
    bool PassJetEtaPtCuts(const xAOD::Jet *jet, const double pt, const double eta);
    void LoadInputTensor();
    static bool SortPt(const xAOD::Jet *jetA, const xAOD::Jet *jetB);


    // CARLNN related data members and functions
    //CARLNN object
    CARLNN* m_CARLNN; //Pointer as want to store on heap due to potential size issue
    //std::map<std::string, float> m_input_tensor_values;
    std::unordered_map<std::string, float> m_input_tensor_values;

    // File data members
    /// User specified data member
    std::vector<double> inputs;
    //std::map<std::string, std::map<std::string, double> > inputs;
    std::map<std::string, std::map<std::string, std::vector<double>> > input_sequences;
    std::string m_sampleName;
    std::string in_file_name;
    std::stringstream paths;
    std::string segment;
    std::string fileLocation;
    std::ifstream infile;
    std::ifstream input;
    std::string output_main;
    std::string DataPath;


    
};

#endif //> !CARLATHENA_CARLATHENAALG_H
