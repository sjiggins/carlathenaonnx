// CARLAthena includes
#include "CARLAthenaAlg.h"
#include "PathResolver/PathResolver.h"

CARLAthenaAlg::CARLAthenaAlg( const std::string &name, ISvcLocator *pSvcLocator )
  : AthAnalysisAlgorithm( name, pSvcLocator )
{
  // User specified flags
  declareProperty("SampleName", m_sampleName = "Dummy", "Descriptive name for the processed sample");
  declareProperty("doDebug", m_doDebug = false);
  declareProperty("doInference", m_doInference = false);
  declareProperty("TreePrefix", m_treePrefix = "Tree", "TTree name prefix");
  declareProperty("TreeDescription", m_treeDescription = "", "TTree description");
}

CARLAthenaAlg::~CARLAthenaAlg()
{
  // no need to delete new allocated TObject since destructor is called from histSvc?
  //delete m_sumWeight;
  //delete m_tree;
}

StatusCode CARLAthenaAlg::initialize()
{

  //===============================================
  //                 (1)
  //===============================================
  // Debug statement based on user request
  if(m_doDebug)
  {
    ATH_MSG_INFO ("Initializing " << name() << "...");
  }
  //===============================================

  
  //===============================================
  //                  (2)
  //===============================================
  // constructing TTree, branches, and histograms
  m_sumWeight = new TH1D("sumWeight", "sumWeight", 1, 1, 1);
  m_tree = new TTree(m_treePrefix.c_str(), m_treeDescription.c_str());

  m_tree->Branch("VpT",&m_VpT);
  m_tree->Branch("Veta",&m_Veta);
  m_tree->Branch("Vmass",&m_Vmass);

  m_tree->Branch("nLeptons",&m_nLeptons);
  m_tree->Branch("Lepton_Pt",&m_Lepton_Pt);
  m_tree->Branch("Lepton_Eta",&m_Lepton_Eta);
  m_tree->Branch("Lepton_Phi",&m_Lepton_Phi);
  m_tree->Branch("Lepton_Mass",&m_Lepton_Mass);
  m_tree->Branch("Lepton_ID",&m_Lepton_ID);

  m_tree->Branch("HT",&m_HT);
  m_tree->Branch("Njets",&m_Njets);
  m_tree->Branch("Jet_Pt",&m_Jet_Pt);
  m_tree->Branch("Jet_Eta",&m_Jet_Eta);
  m_tree->Branch("Jet_Phi",&m_Jet_Phi);
  m_tree->Branch("Jet_Mass",&m_Jet_Mass);

  m_tree->Branch("eventWeight", &m_eventWeight);
  //===============================================

  //===============================================
  //                   (3) 
  //===============================================
  // Register TObjects through histSvc
  // You don't need to construct your histSvc, since it is inherented from parent.
  // for more reference on histSvc,
  // see https://github.com/atlas-org/gaudi/blob/master/GaudiKernel/GaudiKernel/ITHistSvc.h
  std::string path = "/"+m_sampleName+"/"; // this should match the output ROOT file name
  CHECK(histSvc()->regTree(path+m_treePrefix, m_tree));
  CHECK(histSvc()->regHist(path+m_sumWeight->GetName(), m_sumWeight));
  //===============================================


  //===============================================
  //                   (4)
  //===============================================
  if(m_doInference)
  {
    //--------------------------------------------------------
    //Modular version
    //std::string model_path = PathResolver::find_file ("CARLAthena/CKKW20_carl_100000_new.onnx", "DATAPATH");
    std::string model_path = PathResolver::find_file ("CARLAthena/CKKW20_carl_1000000_new.onnx", "DATAPATH");
    m_CARLNN = new CARLNN(model_path, &m_input_tensor_values, m_doDebug);
    //--------------------------------------------------------
    m_tree->Branch("weight",&m_carl_weight); //Add a new branch
  }
  //===============================================


  // Return from initialize()
  return StatusCode::SUCCESS;
}

StatusCode CARLAthenaAlg::finalize()
{
  if(m_doDebug){ ATH_MSG_INFO ("Finalizing " << name() << "...");}

    // Delete the environment object.
  if(m_doInference){
    //----------------------------------------------------
    //Modular version
    CHECK( m_CARLNN->finalise() );
    //delete m_CARLNN;
    //----------------------------------------------------    
  }


  if(m_doDebug){ ATH_MSG_DEBUG( "Ort::Env object deleted" );}
  return StatusCode::SUCCESS;
}

StatusCode CARLAthenaAlg::execute()
{
  
  // Event counter
  m_event_counter++;

  // Print progress based on user requirements
  if( m_doDebug && m_event_counter % 5000 == 0){
    ATH_MSG_DEBUG(name() << " processed events: " << m_event_counter);
  }
  
  //============================================================================
  // retrieve TruthParticles, Jets, and other containner
  CHECK( evtStore()->retrieve(m_truthParticles, "TruthParticles"));
  CHECK( evtStore()->retrieve(m_truthJets, "AntiKt4TruthDressedWZJets"));
  CHECK( evtStore()->retrieve(m_eventInfo, "EventInfo"));

  //============================================================================
  // storing the event weight
  m_eventWeight = m_eventInfo->mcEventWeight();
  m_sumWeight->Fill(1, m_eventWeight); // We should use labels for nominal and alterantive samples for future

  //============================================================================
  // Loop through the truth jet container and filter it with pt & eta cuts
  std::vector<const xAOD::Jet*> SignalJets;
  for(const auto &jet : (*m_truthJets)){
    if(PassJetEtaPtCuts(jet, 10e3, 4.5)){
      SignalJets.push_back(jet);
    }
  }

  // Sort according to decending pt ordering, so the first entry is the leading jet
  std::sort(SignalJets.begin(), SignalJets.end(), SortPt);

  // Storing the jets into branch buffer
  // and construct Scalar sum of pT
  for(const auto &jet : SignalJets){
    m_Jet_Pt.push_back(jet->pt()*1e-3);
    m_Jet_Eta.push_back(jet->eta());
    m_Jet_Phi.push_back(jet->phi());
    m_Jet_Mass.push_back(jet->m()*1e-3);
    m_HT += jet->p4().Pt();
  }
  m_Njets = SignalJets.size();

  //============================================================================
  // Get the leptons and boson
  TLorentzVector boson;
  for(const auto &t_particle : (*m_truthParticles)){
    if(t_particle->status() == 3){  // status code for hard interaction in Sherpa
      auto pdgId = t_particle->pdgId();
      if(abs(pdgId) == 11 || abs(pdgId) == 13){
        m_Lepton_Pt.push_back( t_particle->pt()*1e-3);
        m_Lepton_Eta.push_back( t_particle->eta() );
        m_Lepton_Phi.push_back( t_particle->phi());
        m_Lepton_Mass.push_back( t_particle->m()*1e-3);
        m_Lepton_ID.push_back( pdgId );
        boson += t_particle->p4();
      }
      else{
        continue;
      }
    }
  }
  m_nLeptons = m_Lepton_Pt.size();

  // Convert to GeV
  m_VpT = boson.Pt()*1e-3;
  m_Veta = boson.Eta();
  m_Vmass = boson.M()*1e-3;

  //============================================================================
  // TODO:
  // this part evaluate the carl weight
  // based on the given event kinematic and properties
  if(m_doInference){
    
    // Construct input tensor for CARLNN
    LoadInputTensor();

    m_CARLNN->ValidateMetaAndInputData();
    float CARLweight = m_CARLNN->evaluate();
    m_carl_weight = CARLweight;
    if(m_doDebug){ ATH_MSG_INFO("Weight w is r^{-1}: " << CARLweight); }
    
  }

  //============================================================================
  setFilterPassed(true); //if got here, assume that means algorithm passed
  TreeFill(); //Fill the TTree with the relevant information

  return StatusCode::SUCCESS;
}

StatusCode CARLAthenaAlg::beginInputFile()
{
  //
  //This method is called at the start of each input file, even if
  //the input file contains no events. Accumulate metadata information here
  //

  //example of retrieval of CutBookkeepers: (remember you will need to include the necessary header files and use statements in requirements file)
  // const xAOD::CutBookkeeperContainer* bks = 0;
  // CHECK( inputMetaStore()->retrieve(bks, "CutBookkeepers") );

  //example of IOVMetaData retrieval (see https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/AthAnalysisBase#How_to_access_file_metadata_in_C)
  //float beamEnergy(0); CHECK( retrieveMetadata("/TagInfo","beam_energy",beamEnergy) );
  //std::vector<float> bunchPattern; CHECK( retrieveMetadata("/Digitiation/Parameters","BeamIntensityPattern",bunchPattern) );


  return StatusCode::SUCCESS;
}

void CARLAthenaAlg::TreeFill()
{

  // Fill the tree
  m_tree->Fill();

  // Reset branches
  m_VpT = 0;
  m_Veta = 0;
  m_Vmass = 0;

  m_HT = 0;
  m_Njets = 0;
  m_Jet_Pt.clear();
  m_Jet_Eta.clear();
  m_Jet_Phi.clear();
  m_Jet_Mass.clear();

  m_nLeptons = 0;
  m_Lepton_Pt.clear();
  m_Lepton_Eta.clear();
  m_Lepton_Phi.clear();
  m_Lepton_Mass.clear();
  m_Lepton_ID.clear();

  m_eventWeight = 0;
}



void CARLAthenaAlg::LoadInputTensor(){

  // Loop through TTree branches and add an entry
  TObjArray *branchList; 
  branchList  = m_tree->GetListOfBranches();
  int nBranch     = m_tree->GetNbranches();
  //TString varnames[nBranch];
  for( int i = 0; i < nBranch; i++ ){
    m_input_tensor_values[branchList->At(i)->GetName()] = m_tree->GetBranch(branchList->At(i)->GetName())->GetLeaf(branchList->At(i)->GetName())->GetValue();
  }

}

bool CARLAthenaAlg::PassLeptonEtaPtCuts(const xAOD::TruthParticle *particle, const double pt, const double eta)
{
  if(particle->pt() > pt && fabs(particle->eta()) < eta) return true;
  else return false;
}

bool CARLAthenaAlg::PassJetEtaPtCuts(const xAOD::Jet *jet, const double pt, const double eta)
{
  if(jet->pt() > pt && fabs(jet->eta()) < eta) return true;
  else return false;
}

// Can we make this more generic based on IParticle objects? - sjiggins
bool CARLAthenaAlg::SortPt(const xAOD::Jet *jetA, const xAOD::Jet *jetB)
{
  return jetA->pt() > jetB->pt();
}
