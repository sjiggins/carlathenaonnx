# See: https://twiki.cern.ch/twiki/bin/viewauth/AtlasComputing/SoftwareTutorialxAODAnalysisInCMake for more details about anything here

# this import is required for reading of POOL files (e.g. xAODs)
import AthenaPoolCnvSvc.ReadAthenaPool

from AthenaCommon import CfgMgr

for f in jps.AthenaCommonFlags.FilesInput.StoredValue:
    print("found file: {}".format(f))

# Create the algorithm's configuration.
# gets the main AthSequencer
algseq = CfgMgr.AthSequencer("AthAlgSeq")

# create the output streams handler.
# note: svcMgr is initialized in the athena wrapped python
if not hasattr(svcMgr, "THistSvc"):
    svcMgr += CfgMgr.THistSvc()

# Create the algorithm's configuration.
CARLAthenaAlg = CfgMgr.CARLAthenaAlg(
    SampleName="CARLDataset",
    TreePrefix="Tree",
    TreeDescription="from TRUTH1 Derivation",
    doInference=False,  #Flag for running CARLNN
    doDebug=False,
)

# construct ouput file
svcMgr.THistSvc.Output += [
    "{name} DATAFILE='{name}.root' OPT='RECREATE'".format(name=CARLAthenaAlg.SampleName)
]

# add to the main algorithm sequence
algseq += CARLAthenaAlg

# limit the number of events (for testing purposes)
theApp.EvtMax = -1

# optional include for reducing printout from athena
# include("AthAnalysisBaseComps/SuppressLogging.py")
include("CARLAthena/SuppressLogging.py")
