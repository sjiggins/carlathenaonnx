# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
#
# Configuration for building ONNX Runtime as part of this analysis project.
#

# Extra arguments for the build configuration.
set( _extraArgs )
if( "${CMAKE_BUILD_TYPE}" STREQUAL "Release" OR
      "${CMAKE_BUILD_TYPE}" STREQUAL "RelWithDebInfo" )
   list( APPEND _extraArgs -DCMAKE_BUILD_TYPE:STRING=Release )
elseif( NOT "${CMAKE_BUILD_TYPE}" STREQUAL "" )
   list( APPEND _extraArgs -DCMAKE_BUILD_TYPE:STRING=${CMAKE_BUILD_TYPE} )
endif()
if( NOT "${CMAKE_CXX_STANDARD}" STREQUAL "" )
   list( APPEND _extraArgs -DCMAKE_CXX_STANDARD:STRING=${CMAKE_CXX_STANDARD} )
endif()

# Find all necessary externals for the OnnxRuntime build.
find_package( PythonInterp )
find_package( ZLIB )
find_package( PNG )

# Directory to perform the OnnxRuntime build in.
set( _buildDir
   "${CMAKE_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/onnxruntime" )
# The library produced by onnxruntime.
set( ONNXRUNTIME_LIBRARY
   "${_buildDir}/install/lib/${CMAKE_SHARED_LIBRARY_PREFIX}onnxruntime${CMAKE_SHARED_LIBRARY_SUFFIX}" )

# Build onnxruntime.
ExternalProject_Add( onnxruntime
   PREFIX "${_buildDir}"
   INSTALL_DIR "${_buildDir}/install"
   URL "http://cern.ch/atlas-software-dist-eos/externals/onnxruntime/onnxruntime-v1.5.1.tar.bz2"
   URL_MD5 "bd17c0d085c13bab112e7d92c68b091f"
   SOURCE_SUBDIR cmake
   PATCH_COMMAND patch -p1 <
   ${CMAKE_SOURCE_DIR}/cmake/onnxruntime-1.5.1-cmake.patch
#   COMMAND patch -p1 <
#   ${CMAKE_SOURCE_DIR}/cmake/onnxruntime-1.1.1-automl_featurizers.patch
   CMAKE_CACHE_ARGS -DCMAKE_INSTALL_PREFIX:PATH=<INSTALL_DIR>
   -DCMAKE_INSTALL_INCLUDEDIR:PATH=include
   -DCMAKE_INSTALL_LIBDIR:PATH=lib
   -Donnxruntime_BUILD_SHARED_LIB:BOOL=ON
   -Donnxruntime_BUILD_UNIT_TESTS:BOOL=OFF
   -DPYTHON_EXECUTABLE:FILEPATH=${PYTHON_EXECUTABLE}
   -DZLIB_INCLUDE_DIR:PATH=${ZLIB_INCLUDE_DIR}
   -DZLIB_LIBRARY:FILEPATH=${ZLIB_LIBRARY}
   -DPNG_PNG_INCLUDE_DIR:PATH=${PNG_PNG_INCLUDE_DIR}
   -DPNG_LIBRARY:FILEPATH=${PNG_LIBRARY}
   ${_extraArgs}
   BUILD_BYPRODUCTS "${ONNXRUNTIME_LIBRARY}"
   LOG_CONFIGURE 1 )
ExternalProject_Add_Step( onnxruntime buildinstall
   COMMAND ${CMAKE_COMMAND} -E copy_directory <INSTALL_DIR>
   ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}/
   COMMENT "Installing onnxruntime into the build area"
   DEPENDEES install )

# Install onnxruntime with the analysis project.
install( DIRECTORY "${_buildDir}/install/"
   DESTINATION . USE_SOURCE_PERMISSIONS OPTIONAL )
install( FILES cmake/Findonnxruntime.cmake
   DESTINATION ${CMAKE_INSTALL_CMAKEDIR}/modules )

# Set up the variables to use during the build in order to use OnnxRuntime.
set( ONNXRUNTIME_INCLUDE_DIRS
   $<BUILD_INTERFACE:${_buildDir}/install/include/onnxruntime>
   $<INSTALL_INTERFACE:include/onnxruntime> )
set( ONNXRUNTIME_LIBRARIES
   $<BUILD_INTERFACE:${ONNXRUNTIME_LIBRARY}>
   $<INSTALL_INTERFACE:lib/${CMAKE_SHARED_LIBRARY_PREFIX}onnxruntime${CMAKE_SHARED_LIBRARY_SUFFIX}> )

# Clean up.
unset( _extraArgs )
unset( _buildDir )
